const garage = {
    "count": 1,
    "cars":[{'reg':'AA19 AAA'},{'rag':'AA19EEE'},{}]
};

export const Garage = {   
    add(value){
        let found = true;
        Garage.removeInvalid();
        // checking whether value has reg in it
        if (!('reg' in value)){
            return;
        }
        // checking whether this value already exists to stop duplicates
        const regValue = value.reg.replace(/\s/g, '').toUpperCase();
        garage.cars.map((car) => {
            if (car){
                if (!('reg' in car)){
                    found = false;
                }
                let storedValue = car.reg.replace(/\s/g, '').toUpperCase();
                if (regValue === storedValue){
                    found = false;
                } 
            }
        })
        if (found){
            garage.count++;
            garage.cars[garage.count] = value;
            return true;
        }
        return false;
    },
    delete(reg){
        Garage.removeInvalid()
        let carToDelete = null;
        const regValue = reg.replace(/\s/g, '').toUpperCase();
        garage.cars.map((car, idx) => {
            if ('reg' in car){
                let storedValue = car.reg.replace(/\s/g, '').toUpperCase();
                if (regValue === storedValue){
                    carToDelete = garage.cars.splice(idx, 1)[0];
                } 
            }
        })
        return carToDelete;
    },
    get(reg){
        Garage.removeInvalid()
        let returnedCar = null;
        const regValue = reg.replace(/\s/g, '').toUpperCase();
        garage.cars.map((car) => {
            if ('reg' in car){
                let storedValue = car.reg.replace(/\s/g, '').toUpperCase();
                if(regValue === storedValue){
                    returnedCar = car;
                }
            }
        })
        return returnedCar
    },
    getAll(){
        return garage.cars;
    },
    removeInvalid(){
        garage.cars = garage.cars.filter((car)=>{
            if ('reg' in car){
                return true;
            }
            return false;
        })
        garage.count = garage.cars.length;
    }
}