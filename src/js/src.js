import { Garage } from "./garage";
const axios = require("axios")

window.addEventListener("garage-loaded", start, false);
let container = document.createElement("div")
async function start() {

    Garage.removeInvalid()
    displayInput()

    document.body.className = "container"
    container.className="card_container"

    document.body.appendChild(container)


    const cars = Garage.getAll()

    cars.forEach(async car => {
    var data = JSON.stringify({ registrationNumber: car.reg});
    axios({
        method: 'post',
        url:
        "https://cors-anywhere.herokuapp.com/https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles",
        headers: {
          'x-api-key': 'mG1zaRgSH21lGk5mHwqgV6Y4oGkm8UpX5VNbfHoN',
          'Content-Type': 'application/json',
        },
        data: data,
      })
      .then(data => {
        // Extract the required information from the response and update the car object
        const carData = data.data
        if (carData.make) {
            car.make = carData.make;
        } 
        if (carData.engineCapacity){
            car.model1 = carData.engineCapacity+"cc"
        }
        if (carData.fuelType){
            car.model2 = carData.fuelType
        }if (carData.wheelplan){
            car.model3 = carData.wheelplan
        }
        if (carData.yearOfManufacture){
            car.year=carData.yearOfManufacture
        }
        // You can also update other information such as image if available

        // Display the car information on the page
        displayCarInformation(car);
      })
      .catch(error => {
        // Handle any errors that occurred during the API request
        console.error('API Error:', error);
        displayCarInformation(car);
      });
  });


}

function displayCarInformation(car) {
        const carElement = document.createElement('div')
        carElement.innerHTML = `
          <div class="car_card">
          <img src="${car.image ? car.image : "https://maxler.com/local/templates/maxler/assets/img/not-found.png"}" class="img_container">
          <h2>${car.reg}</h2>
          <p>Make: ${car.make}</p>
          <p>Model: ${car.model1}</p>
          <p>${car.model2}</p>
          <p>${car.model3}</p>
          <p>Year: ${car.year}</p>
          </div>
        `;
        container.appendChild(carElement)
    
    // Assuming you have HTML elements with IDs to display the car information
  }


function displayInput(){
    const regtext = document.createElement("div")
    const input = document.createElement("input")
    regtext.innerHTML = "<p>Enter your reg Number:</p>"
    regtext.className = "center-text"
    regtext.appendChild(input)
    const button = document.createElement("button")
    button.textContent="Submit"
    regtext.appendChild(button)
    button.addEventListener("click", (ev)=>{
        axios({
            method: 'post',
            url:
              "https://cors-anywhere.herokuapp.com/https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles",
            headers: {
              'x-api-key': 'mG1zaRgSH21lGk5mHwqgV6Y4oGkm8UpX5VNbfHoN',
              'Content-Type': 'application/json',
            },
            data: {registrationNumber: input.value},
          })
          .then(data => {
            // Extract the required information from the response and update the car object
            const carData = data.data
            const car = {reg: carData.registrationNumber }
            if (carData.make) {
                car.make = carData.make;
            } 
            if (carData.engineCapacity){
                car.model1 = carData.engineCapacity+"cc"
            }
            if (carData.fuelType){
                car.model2 = carData.fuelType
            }if (carData.wheelplan){
                car.model3 = carData.wheelplan
            }
            if (carData.yearOfManufacture){
                car.year=carData.yearOfManufacture
            }
            // You can also update other information such as image if available
    
            // Display the car information on the page
            if (Garage.add(car)){
                displayCarInformation(car);
            }
          })
          .catch(error => {
            // Handle any errors that occurred during the API request
            console.error('API Error:', error);
          });
    })
    document.body.appendChild(regtext)
}
